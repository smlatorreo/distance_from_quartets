# distance_from_quartets

This program calculates quartet-based genetic distances from a set of taxa.

It is a simplified version of the SVD-quartets algorithm (Chifman and Kubatko, 2014) since it assumes equal base frequencies and equal mutation rates (Jukes and Cantor, 1969) bypassing the calculation of SVD scores per quartet.
Instead, this algorithm assumes the most likely quartet configuration per variable site, based on the most frequent biallelic segregation pattern from the three following possible configurations:

```
i) 1       3        ii) 1       2       iii) 1       2
    \_____/              \_____/              \_____/
    /     \              /     \              /     \
   2       4            3       4            4       3
```

SNP configurations supporting each topology:

  i: 1100 / 0011

 ii: 1010 / 0101

iii: 1001 / 0110

, where 1 and 0 are REF and ALT alleles, respectively.

The input is expected to be a pseudo fasta format file, composed by a genomic set of SNPs. Hence, sequences should have the same length.
The output is a symmetric distance matrix.

USAGE:
```bash
python3 distances_quartets.py <input.fasta> <n_cores> > <output.file>
```
