#!/usr/bin/env python3
#
# USAGE: python3 distances_quartets.py <input.fasta> <n_cores> > <output.file>

__author__ = "Sergio Latorre"
__license__ = "GPL"
__email__ = "sergio.latorre@tuebingen.mpg.de"

from sys import argv
import multiprocessing as mp
from itertools import combinations

def readfasta(file):
    sequences = {}
    with open(file, 'r') as f:
        for line in f:
            sequence = ''
            if line.startswith('>'):
                nameseq = line.split('>')[1].strip()
            else:
                sequence = sequence + line.strip()
            sequences[nameseq] = sequence
    return(sequences)

def assessrel(site):
    if (site.count('A'), site.count('T'), site.count('C'), site.count('G')).count(0) != 2:
        return(0)
    else:
        sitebin = ''
        for n in site:
            if n == list(set(site))[0]:
                sitebin = sitebin + '0'
            else:
                sitebin = sitebin + '1'
        if sitebin == '0011' or sitebin == '1100':
            return(1)
        elif sitebin == '0101' or sitebin == '1010':
            return(2)
        elif sitebin == '1001' or sitebin == '0110':
            return(3)
        else:
            return(0)

sequences = readfasta(argv[1])

def evalquartets(quartet):
    topologies = {1:(quartet[0], quartet[1], quartet[2], quartet[3]),
                  2:(quartet[0], quartet[2], quartet[1], quartet[3]),
                  3:(quartet[0], quartet[3], quartet[1], quartet[2])}
    freqs = {1:0, 2:0, 3:0, 0:0}
    for n in range(len(sequences[quartet[0]])):
        site = ''.join([sequences[quartet[i]][n] for i in range(4)])
        freqs[assessrel(site)] += 1
    freqs.pop(0) # Remove non-informative configurations
    total = sum(freqs.values())
    maxtop = max(freqs, key=freqs.get) # Highest supported topology
    return((topologies[maxtop], freqs[maxtop] / total))

pool = mp.Pool(int(argv[2]))
out = pool.map(evalquartets, [quartet for quartet in (combinations(sequences.keys(), 4))])
pool.close()

def pairdist(quartet, tax1, tax2):
    if tax1 not in quartet and tax2 not in quartet:
        return(0)
    else:
        part1 = (quartet[0], quartet[1])
        part2 = (quartet[2], quartet[3])
        if tax1 in part1 and tax2 in part2:
            return(1)
        elif tax1 in part2 and tax2 in part1:
            return(1)
        else:
            return(0)

quartets = [quartet[0] for quartet in out]
pdistances = {}
for pair in (combinations(sequences.keys(), 2)):
    subsetquartets = []
    for quartet in quartets:
        if pair[0] in quartet and pair[1] in quartet:
            subsetquartets.append(quartet)
    results = [pairdist(quartet, pair[0], pair[1]) for quartet in subsetquartets]
    pdistances[pair] = sum(results)

print(*(list(sequences.keys())), sep = '\t')
for pos in range(len(sequences.keys())):
    sample = list(sequences.keys())[pos]
    d = []
    for pair in pdistances:
        if sample in pair:
            d.append(pdistances[pair])
    d.insert(pos, 0)
    print(*d, sep = '\t')

